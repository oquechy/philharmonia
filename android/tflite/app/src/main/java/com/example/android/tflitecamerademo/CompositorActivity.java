package com.example.android.tflitecamerademo;

import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static com.example.android.tflitecamerademo.CameraActivity.HOST;

public class CompositorActivity extends AppCompatActivity {
    private boolean playerEnabled = false;
    private String name;
    private JSONArray composerSongs = null;
    private int curSong = 0;
    private List<MediaPlayer> tracks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        name = getIntent().getStringExtra("name");

        setContentView(R.layout.activity_compositor);
        initView(name);
    }

    protected void initView(String name) {
        ImageView photo = (ImageView) findViewById(R.id.imageView);
        int id = getResources().getIdentifier(name, "drawable", getPackageName());
        photo.setImageResource(id);
        TextView title = (TextView) findViewById(R.id.title);
        boolean titleIsSet = false;
        try {
            title.setText(new HttpGetRequest().execute(HOST + "name/" + name).get());
            titleIsSet = true;
        } catch (InterruptedException e) {
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        if (!titleIsSet) {
            title.setText(name.equals("tchaikovsky") ? "Пётр Чайковский" : "Сергей Рахманинов");
        }

        TextView bio = (TextView) findViewById(R.id.description);
        try {
            final InputStream in = getAssets().open(name + ".txt");
            bio.setText(Utils.readStream(in));
        } catch (IOException e) {
            e.printStackTrace();
        }

        String data = null;
        try {
            data = new HttpGetRequest().execute(HOST + "songs").get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        if (data != null) {
            try {
                JSONObject songs = new JSONObject(data);
                composerSongs = songs.getJSONArray(name);
                tracks = new ArrayList<>();
                for (int i = 0; i < composerSongs.length(); i++) {
                    tracks.add(new MediaPlayer());
                    tracks.get(i).setAudioStreamType(AudioManager.STREAM_MUSIC);
                    tracks.get(i).setDataSource(HOST + "music/" + ((JSONObject)
                            (composerSongs.get(i))).getString("en"));
                    tracks.get(i).prepareAsync();
                }
            } catch (JSONException | IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void onPlayClicked(View v) {
        if (composerSongs == null || tracks == null || tracks.size() == 0) {
            Toast.makeText(this, "Не получилось загрузить композиции", Toast.LENGTH_SHORT).show();
            return;
        }
        if (playerEnabled) {
            playerEnabled = false;
            tracks.get(curSong).pause();
            try {
                System.out.println("Disabling");
                System.out.println(((JSONObject)
                        (composerSongs.get(curSong))).getString("en"));
                TextView labelView = (TextView) findViewById(R.id.label);
                labelView.setText("");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            FloatingActionButton playButton = (FloatingActionButton) findViewById(R.id.play);
            playButton.setImageResource(R.drawable.play);
        } else {
            playerEnabled = true;
            tracks.get(curSong).start();

            try {
                System.out.println("Enabling");
                System.out.println(((JSONObject)
                        (composerSongs.get(curSong))).getString("en"));
                String label = ((JSONObject)
                        (composerSongs.get(curSong))).getString("ru");
                TextView labelView = (TextView) findViewById(R.id.label);
                labelView.setText(label);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            FloatingActionButton playButton = (FloatingActionButton) findViewById(R.id.play);
            playButton.setImageResource(R.drawable.pause);
        }
    }

    public void myTicketsHandler(View view) {
        Intent intent = new Intent(this, AllConcertsActivity.class);
        intent.putExtra("name", name);
        startActivity(intent);
    }

    public void onNextClicked(View view) {
        if (tracks == null || tracks.size() == 0) {
            return;
        }
        stopCurrent();
        curSong = (curSong + 1) % tracks.size();
        try {
            System.out.println("Switch to");
            System.out.println(((JSONObject)
                    (composerSongs.get(curSong))).getString("en"));
            String label = ((JSONObject)
                    (composerSongs.get(curSong))).getString("ru");
            TextView labelView = (TextView) findViewById(R.id.label);
            labelView.setText(label);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void onPrevClicked(View view) {
        if (tracks == null || tracks.size() == 0) {
            return;
        }
        stopCurrent();
        curSong = (curSong - 1 + tracks.size()) % tracks.size();
        try {
            System.out.println("Switch to");
            System.out.println(((JSONObject)
                    (composerSongs.get(curSong))).getString("en"));
            String label = ((JSONObject)
                    (composerSongs.get(curSong))).getString("ru");
            TextView labelView = (TextView) findViewById(R.id.label);
            labelView.setText(label);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void stopCurrent() {
        if (tracks.get(curSong).isPlaying()) {
            tracks.get(curSong).stop();
            tracks.get(curSong).prepareAsync();
            FloatingActionButton playButton = (FloatingActionButton) findViewById(R.id.play);
            playButton.setImageResource(R.drawable.play);
        }
    }
}
