package com.example.android.tflitecamerademo;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.widget.ImageView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import jp.wasabeef.blurry.Blurry;

public class Utils {
    public static String readStream(InputStream in) throws IOException {
        BufferedReader r = new BufferedReader(new InputStreamReader(in));
        StringBuilder total = new StringBuilder();
        for (String line; (line = r.readLine()) != null; ) {
            total.append(line).append('\n');
        }
        return total.toString();
    }

    public static void blur(Context context, ImageView view, int id) {
        view.post(() -> Blurry.with(context)
                .radius(25)
                .sampling(2)
                .color(Color.argb(200, 10, 10, 10))
                .async()
                .animate()
                .from(BitmapFactory.decodeResource(context.getResources(), id))
//                .capture(view)
                .into(view));
    }
}
