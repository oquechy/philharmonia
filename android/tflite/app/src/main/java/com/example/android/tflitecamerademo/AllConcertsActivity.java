package com.example.android.tflitecamerademo;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import static com.example.android.tflitecamerademo.Utils.blur;

public class AllConcertsActivity extends AppCompatActivity {

    private ListView concerts;
    private String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().setStatusBarColor(ContextCompat.getColor(this, android.R.color.black));
        setContentView(R.layout.activity_all_concerts);

        name = getIntent().getStringExtra("name");
        setTitle("Aфиша концертов");

        System.out.println(getConcertInfo(name));
        ArrayList<JSONObject> names = new ArrayList<>();
        String data = getConcertInfo(name);
        if (data == null) {
            Toast.makeText(this, "Невозможно узнать расписание концертов, пожалуйста, проверьте " +
                    "наличие соединения с Интернетом.", Toast.LENGTH_LONG).show();
        } else {
            try {
                JSONArray info = new JSONArray(data);
                for (int i = 0; i < info.length(); i++) {
                    names.add(((JSONObject) info.get(i)));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        concerts = (ListView) findViewById(R.id.concerts);

        // call getInfo
        MySimpleArrayAdapter adapter = new MySimpleArrayAdapter(getApplicationContext(), names);
        concerts.setAdapter(adapter);
        concerts.setOnItemClickListener((adapterView, view, i, l) -> {

        });
    }

    private String getConcertInfo(String name) {
        String path = CameraActivity.HOST + "concerts/" + name;

        try {
            return new HttpGetRequest().execute(path).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    public class MySimpleArrayAdapter extends ArrayAdapter<JSONObject> {
        private final Context context;
        private final ArrayList<JSONObject> values;

        public MySimpleArrayAdapter(Context context, ArrayList<JSONObject> values) {
            super(context, -1, values);
            this.context = context;
            this.values = values;
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.card, parent, false);
            TextView datetime = rowView.findViewById(R.id.datetime);
            TextView description = rowView.findViewById(R.id.concert_description);
            TextView concert_name = rowView.findViewById(R.id.concert_name);
            ImageButton tickets = rowView.findViewById(R.id.tickets);
            ImageView picture = rowView.findViewById(R.id.list_image);
            try {
                String month = formatDate(position);
                datetime.setText(month + "\n" + values.get(position).getString("время, чч-мм"));
                description.setText(values.get(position).getString("программа"));
                concert_name.setText(values.get(position).getString("title"));
                tickets.setOnClickListener(view -> {
                    Intent browserIntent = null;
                    try {
                        browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(values.get
                                (position).getString("купить билет")));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    startActivity(browserIntent);
                });

                String pics = values.get(position).getString("ссылки на фото участников");
                if (pics.length() > 0) {
                    String path = pics.split("\n")[0];
                    Picasso.with(context).load(path).into(picture);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return rowView;
        }

        private String formatDate(int position) throws JSONException {
            Date date = new Date();
            SimpleDateFormat simpleDateFormat =
                    new SimpleDateFormat("yyyy-MM-dd", new Locale("ru"));
            try {
                date = simpleDateFormat.parse(values.get(position).getString("дата, " +
                        "гггг-мм-дд"));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Locale russian = new Locale("ru");
            String[] newMonths = {
                    "января", "февраля", "марта", "апреля", "мая", "июня",
                    "июля", "августа", "сентября", "октября", "ноября", "декабря"};
            DateFormatSymbols dfs = DateFormatSymbols.getInstance(russian);
            dfs.setMonths(newMonths);
            DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT, russian);
            SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM", russian);
//            sdf.applyPattern(sdf.toPattern().replaceAll("[^\\p{Alpha}]*y+[^\\p{Alpha}]*", ""));
            sdf.setDateFormatSymbols(dfs);

            return sdf.format(date);
        }
    }
}
